if (!FireOTR) 
    var FireOTR = {};


FireOTR.message = function  (to, text)
{
    this.to = to;
    this.text = text;
}


FireOTR.fb_handler = 
{

    sendDefaultQuery : function (to)
    {
	var msg = new ctypes.char.ptr;
	msg = FireOTR.libotr.otrl_proto_default_query_msg (FireOTR.fb_handler.fbid.toString(), 59);
	FireOTR.fb_handler.enqueueMessage (to, msg.readString ()); 
    },

    gotNewMessage : function (from, msg_txt)
    {
	try{
	    fbid = FireOTR.fb_handler.fbid
	    if (from == fbid) //we are the sender of the message
		return;

	    FireOTR.processingReceivingMessage (fbid, "facebook", from, msg_txt);

	}
	catch (e)
	{
	    FireOTR.debug (e);
	}
    },

    onPageLoad : function (event)
    {
	try{

	    var win = event.originalTarget.defaultView;

	    /* Check if we are on top window */
	    if (win != win.top)
		return
	    
	    
	    FireOTR.fb_handler.fb_doc = win.document;
	    FireOTR.fb_handler.fbid = FireOTR.get_cookie ('c_user', event.originalTarget);


	    /*  TODO Already openedTab should be checked too */
	    var script = "function sendEvents (chat, user){var doc = unsafeWin.document;if (doc && doc.createEvent)var evt = doc.createEvent ('Events');evt.initEvent ('fireotr-added-tab-conv', true, false);chat.chatConv.setAttribute('user', user);chat.chatConv.setAttribute('name',chat.name);chat.chatConv.dispatchEvent (evt);evt = doc.createEvent ('Events');evt.initEvent ('fireotr-added-tab-input', true, false);chat.chatInput.setAttribute ('user', user);chat.chatInput.dispatchEvent (evt);}function setChatWatcher (user){unsafeWin.chatDisplay.tabs.watch (user, function (prop, oldv, newv){sendEvents (newv, user);return newv;});}unsafeWin.watch ('Chat',function  (prop, oldv, newv){var old = newv.openTab;newv.openTab = function (a, b, c){setChatWatcher (a);old (a, b, c);};return newv;});";


	    /*	     CODE ABOVE 
	       function sendEvents (chat, user)
	       {
	       var doc = unsafeWin.document;
	       if (doc && doc.createEvent)
	       var evt = doc.createEvent ('Events');
	       evt.initEvent ('fireotr-added-tab-conv', true, false);
	       chat.chatConv.setAttribute('user', user);
	       chat.chatConv.setAttribute('name',chat.name);
	       chat.chatConv.dispatchEvent (evt);
	       evt = doc.createEvent ('Events');
	       evt.initEvent ('fireotr-added-tab-input', true, false);
	       chat.chatInput.setAttribute ('user', user);
	       chat.chatInput.dispatchEvent (evt);
	       }
	       function setChatWatcher (user)
	       {
	       unsafeWin.chatDisplay.tabs.watch (user, function (prop, oldv, newv)
	       {
	       sendEvents (newv, user);
	       return newv;
	       });
	       }
	       unsafeWin.watch ('Chat',function  (prop, oldv, newv)
				{
		                    var old = newv.openTab;
				    newv.openTab = function (a, b, c)
				    {
					setChatWatcher (a);
					old (a, b, c);
				    };
				    return newv;});
	    */


	    function replaceChatConv (event)
	    {


		var fbConvContainer = event.originalTarget;
		var user = event.originalTarget.getAttribute ('user');
		FireOTR.fb_handler.names[user] = event.originalTarget.getAttribute ('name');
		var chatIF = event.originalTarget.ownerDocument.createElement ('iframe');
		chatIF.setAttribute ('id', 'c_'+user);
		chatIF.setAttribute ('name', 'c_'+user);

		fbConvContainer.parentNode.replaceChild (chatIF, fbConvContainer);
		chatIF.setAttribute ('src','about:blank');
		chatIF.style.height = '200px';
		chatIF.style.width = '250px';

	    }

	    function replaceChatInput (event)
	    {

		var fbTextArea = event.originalTarget;
		var user = event.originalTarget.getAttribute ('user');
		var secureIF = event.originalTarget.ownerDocument.createElement ('iframe');

		fbTextArea.parentNode.replaceChild (secureIF, fbTextArea);
		secureIF.setAttribute ('src','about:blank');
		secureIF.style.height = '50px';
		secureIF.style.width = '250px';
		secureIF.setAttribute ('id', 'f_'+user);

		var a = secureIF.contentDocument.createElement ('textarea');
		a.setAttribute ('id', user);

		/* STUB */
		var encryptButton = secureIF.contentDocument.createElement ('p');
		var text = secureIF.contentDocument.createTextNode ('button');
		encryptButton.appendChild (text);
		/* STUB */

		setTimeout (
		    function()
		    {
			secureIF.contentDocument.body.appendChild (a);
			a.addEventListener ('keydown', FireOTR.fb_handler.onChatTextAreaKeyDown, false);
			secureIF.contentDocument.body.appendChild (encryptButton);
			encryptButton.addEventListener ('click', FireOTR.fb_handler.onEncButtonClicked, false);
			encryptButton.id = user;
		    }
		    ,500); /* Immediately populating the frame doesn't work */


	    }

	    var sandbox = new Components.utils.Sandbox (win);
	    win.document.addEventListener ('fireotr-added-tab-conv', replaceChatConv, false, true);
	    win.document.addEventListener ('fireotr-added-tab-input', replaceChatInput, false, true);
	    sandbox.unsafeWin = win.wrappedJSObject;
	    sandbox.debug = FireOTR.debug;
	    var res = Components.utils.evalInSandbox (script, sandbox);

	    
	     if (!FireOTR.fb_handler.observer)
	     {
	     	FireOTR.fb_handler.observer = new FireOTR.fb_handler.fbobserver ();
	     	FireOTR.fb_handler.observer.register ();
	     }
	    
	}
	catch(e)
	{
	    FireOTR.debug ('Error loading facebook handler:');
	    FireOTR.debug (e);
	}
	    
    },
    
    onChatTextAreaKeyDown : function (event)
    {
	if (event.keyCode == 13)
	{ 
	    FireOTR.fb_handler.sendInput (event.originalTarget);
	}


    },

    onEncButtonClicked : function (event)
    {
	try {
	    var username = event.originalTarget.id;
	    var account = FireOTR.fb_handler.fbid;
	    var protocol = 'facebook';

	    var context = FireOTR.libotr.otrl_context_find (FireOTR.user_state,
							    username,
							    account,
							    protocol,
							    1,
							    null,
							    null,
							    null);
	    if (context.isNull())
	    {
		FireOTR.debug ("Error occured can't find context");
	    }
	    FireOTR.debug (context.contents.msgstate);
	    if (context.contents.msgstate == 0)/*PLAINTEXT*/
	    {
		FireOTR.fb_handler.sendDefaultQuery (username);
	    }
	    else if (context.contents.msgstate == 2)/* FINISHED*/
	    {
		FireOTR.libotr.otrl_context_force_plaintext (context);
		FireOTR.gone_insecure_cb (null, context);
	    }
	    else if  (context.contents.msgstate == 1)/*ENCRPTED*/
	    {
		FireOTR.libotr.otrl_message_disconnect (FireOTR.user_state,
					  		FireOTR.uiOps.address(),
							null,
							account,
							protocol,
							username);
		FireOTR.gone_insecure_cb (null, context);

	    }
	}
	catch (e)
	{
	    FireOTR.debug (e);
	}
    },
    sendInput : function (textarea)
    {

	var msg_text = textarea.value;
	FireOTR.debug (msg_text);
	var to = textarea.getAttribute ('id');
	textarea.value = '';
	var fbid = FireOTR.fb_handler.fbid;

	var convIF = FireOTR.fb_handler.fb_doc.getElementById ('c_'+to);
	var p_node = convIF.contentDocument.createElement ('P');
	var text = convIF.contentDocument.createTextNode ('me# '+msg_text);
	p_node.appendChild (text);
	convIF.contentDocument.body.appendChild
(p_node);

	FireOTR.processingSendingMsg (fbid, 'facebook', to, msg_text);

    },

    nameToConvIF : function (id)
    {

	var convIF = FireOTR.fb_handler.fb_doc.getElementById ('c_'+id);
	if (!convIF)
	    FireOTR.debug  ("Can't find conersion frame for "+id);
	return convIF;
    },

    displayMsg : function (from, msg)
    {
	var convIF = FireOTR.fb_handler.nameToConvIF (from);
	var p_node = convIF.contentDocument.createElement ('P');
	var text = convIF.contentDocument.createTextNode (FireOTR.fb_handler.names[from]+"# "+msg);
	p_node.appendChild (text);
	convIF.contentDocument.body.appendChild(p_node);
    },


    sendNextMessage : function ()
    {
	if (FireOTR.fb_handler.messageQueue.length <= 0)
	    return;

	req_body = FireOTR.fb_handler.makeMessageBody (FireOTR.fb_handler.messageQueue.shift ());
	var req = new XMLHttpRequest ();

	req.open("POST", "http://www.facebook.com/ajax/chat/send.php?__a=1", true);


	req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	req.setRequestHeader("Content-length", req_body.length);
	//	req.setRequestHeader("Connection", "close"); 

	/* We send a message a time */
	req.onreadystatechange = function() {
	    if(req.readyState == 4) {
		FireOTR.fb_handler.pendingMessages--;
		if (FireOTR.fb_handler.pendingMessages == 0 && FireOTR.fb_handler.messageQueue.length > 0)

		    FireOTR.fb_handler.sendNextMessage ();
	    }
	}
	FireOTR.fb_handler.pendingMessages++;
	req.send(req_body);


    },

    enqueueMessage : function (to, text) {
	
	var msg = new FireOTR.message (to, text);
	
	FireOTR.fb_handler.messageQueue.push (msg);
	if (FireOTR.fb_handler.pendingMessages == 0)
	{
	    FireOTR.debug ("enqueue msg :"+msg.text);
	    FireOTR.fb_handler.sendNextMessage ();
	}
    },

    makeMessageBody : function (msg)
    {
	var to = msg.to;
	var message = msg.text;

	var msg_id =  Math.floor(Math.random() * 4294967295) + 1;
	var time = new Date().getTime ();

	var doc = gBrowser.contentDocument;
	var post_form_id = doc.getElementsByName ('post_form_id')[0];

	if (!post_form_id)
	    FireOTR.debug ("Some error, can't find post_form_id");

	post_form_id = post_form_id ? post_form_id.value : 0;
	var fb_dtsg = doc.getElementsByName ('fb_dtsg')[0];
	if (!fb_dtsg)
	    FireOTR.debug ("Some error, can't find post_form_id");

	fb_dtsg = fb_dtsg ? fb_dtsg.value : 0;

	var req_body = "msg_id="+msg_id+"&";
	req_body += "client_time="+time+"&";
	req_body += "to="+to+"&";
	req_body += "num_tabs=1&";
	req_body += "pvs_time&";
	req_body += "msg_text="+encodeURIComponent(message)+"&";
	req_body += "to_offline=false&";
	req_body += "to_idle=false&";
	req_body += "post_form_id="+post_form_id+"&";
	req_body += "fb_dtsg="+fb_dtsg+"&";
	req_body += "post_form_id_source=AsyncRequest";

	
	return req_body;
    },

    gone_secure : function (opdata, context)
    {
	try{
	    FireOTR.debug ('here');
	    var fp = context.contents.active_fingerprint;
	    var protocol = context.contents.protocol.readString ();
	    var username = context.contents.username.readString ();
	    

	    var convIF = FireOTR.fb_handler.nameToConvIF (username);

	    if (!fp.contents.trust.isNull() && fp.contents.trust.readString() == 'verified')
		convIF.style.backgroundColor = 'green';
	    else 
		/* We've to trust the fingerprint */
		convIF.style.backgroundColor = 'grey';
	}
	catch (e)
	{
	    FireOTR.debug (e);
	}

    },

    gone_insecure : function (opdata, context)
    {
	var protocol = context.contents.protocol.readString ();
	var username = context.contents.username.readString ();
	

	var convIF = FireOTR.fb_handler.nameToConvIF (username);
	convIF.style.backgroundColor = 'white';
    },

};


FireOTR.fb_handler.listener = function  ()
{
    this.receivedData = "";
    this.forward = false;
}

FireOTR.fb_handler.listener.prototype = {
    onStartRequest: function (request, context) {
	try{
	    if (this.oldListener)
		this.oldListener.onStartRequest (request, context);
	}
	catch(e)
	{
	    debug (e);
	}
    },

/*thanks to http://www.softwareishard.com/blog/firebug/nsitraceablechannel-intercept-http-traffic/ */
    onDataAvailable: function (req, context, istream, offset, count)
    {
	try{
	    var jsStream = Components.classes["@mozilla.org/scriptableinputstream;1"].createInstance(Components.interfaces.nsIScriptableInputStream);
	    jsStream.init(istream);

	    var tmpData = jsStream.read (count);
	    this.receivedData += tmpData;
            var seekableStream = Components.classes["@mozilla.org/io/string-input-stream;1"];
            seekableStream  = seekableStream.createInstance(Components.interfaces.nsIStringInputStream);
	    seekableStream.setData (tmpData, tmpData.length);
	    seekableStream.QueryInterface (Components.interfaces.nsISeekableStream);
	    seekableStream.seek (0,0);
            this.oldListener.onDataAvailable (req, context, seekableStream, offset, count);

	}
	catch (e)
	{
	    FireOTR.debug (e);
	}
    },

    onStopRequest: function (aRequest, aContext, aStatus)
    {

	try{
            var seekableStream = Components.classes["@mozilla.org/io/string-input-stream;1"];
	    seekableStream  = seekableStream.createInstance(Components.interfaces.nsIStringInputStream);

	    var nativeJSON = Components.classes["@mozilla.org/dom/json;1"].createInstance(Components.interfaces.nsIJSON);
	    var JSObj = nativeJSON.decode (this.receivedData.substr(9));

	    if (JSObj.t == 'msg' && JSObj.ms)
	    {
		for (id in JSObj.ms)
		{

		    var message = JSObj.ms[id];
		    if (message.type == 'msg')
		    {
			var from = "";
			var msg_txt = "";
			
			if (message.msg)
			{
			    from = message.from;
			    msg_txt = message.msg.text;
			}
			else
			{
			    FireOTR.debug ('Can\'t parse message :S ');
			}
			FireOTR.fb_handler.gotNewMessage (from, msg_txt);
		    }
		}
	    }
	    this.oldListener.onStopRequest  (aRequest, aContext, aStatus);
	}
	catch(e)
	{
	    //this.oldListener.onStopRequest  (aRequest, aContext, aStatus);

	    FireOTR.debug (e);
	    FireOTR.debug (this.receivedData);
	}
    },

};


FireOTR.fb_handler.fbobserver = function ()
{
    this.isRegistered = false;
}

FireOTR.fb_handler.fbobserver.prototype = 
    {
	observe : function (aSubject, aTopic, aData)
	{
            if (aTopic == "http-on-examine-response")
	    {
		aSubject.QueryInterface(Components.interfaces.nsIHttpChannel);
		this.onResponse(aSubject);
	    }
	},

	onResponse : function (httpChannel){

	    if (httpChannel.URI.asciiSpec.match (/channel.facebook.com/))
	    {
		try{
		    var myListener = new FireOTR.fb_handler.listener ();
		    httpChannel.QueryInterface (Components.interfaces.nsITraceableChannel);
		    myListener.oldListener = httpChannel.setNewListener (myListener);

		}
		catch (e)
		{
		    FireOTR.debug (e);
		}
	    }
	    
	},

	register : function()
	{
	    var observerService = (Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService));
	    observerService.addObserver (this, "http-on-examine-response", false);	
	    this.isRegistered = true;

	},
	unregister: function()
	{
	    var observerService = (Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService));
//	    observerService.removeObserver (this, "http-on-modify-request");
	    observerService.removeObserver (this, "http-on-examine-response", false);	
	    this.isRegistered = false;
	},
    };


try{
    FireOTR.fb_handler.names = {};
    FireOTR.fb_handler.url_matching = /www\.facebook\.com/;
    FireOTR.fb_handler.protocol = "facebook";
    FireOTR.fb_handler.messageQueue = new Array ();
    FireOTR.fb_handler.pendingMessages = 0;
    /* add the handler to FireOTR */
    FireOTR.handlers[FireOTR.fb_handler.protocol] = FireOTR.fb_handler;
}
catch (e)
{
    FireOTR.debug (e);
}






