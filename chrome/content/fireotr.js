if (!FireOTR) 
    var FireOTR = {};



FireOTR.enabled = true;
FireOTR.handlers = {};
FireOTR.context = {};
/* UTILS */
FireOTR.debug = function(string)
{
    if (Firebug)
	Firebug.Console.log (string);
    else 
	console.log (string);
}

//utils TODO http://www.elated.com/articles/javascript-and-cookies/
FireOTR.get_cookie = function ( cookie_name, doc )
{
  var results = doc.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

  if ( results )
    return ( unescape ( results[2] ) );
  else
    return null;
}


FireOTR.processingSendingMsg = function (name, protocol, to, msg_text)
{
    try{
	var err;
	var newMessage = new ctypes.char.ptr (null);

	err = FireOTR.libotr.otrl_message_sending (FireOTR.user_state,
						  FireOTR.uiOps.address(),
						  null,
						  name,
						  protocol,
						  to,
						  msg_text,
						  null,
						  newMessage.address(),
						  null,
						  null);

	//TODO should be send and fragment
	if (!err && !newMessage.isNull())
	{
	    FireOTR.debug ('err: '+err+' sending message: ' + newMessage.readString ());
	    var context = FireOTR.libotr.otrl_context_find (FireOTR.user_state,
							    to,
							    name,
							    protocol,
							    0, //?
							    null,
							    null,
							    null);
	    FireOTR.debug (context.contents.username.readString());
	    FireOTR.debug (newMessage.readString());
	    FireOTR.libotr.otrl_message_fragment_and_send (FireOTR.uiOps.address(),
							  null,
							  context,
							  newMessage,
							  FireOTR.libotr.OTRL_FRAGMENT_SEND_ALL,
							  null);
							  
							  
	    FireOTR.libotr.otrl_message_free (newMessage);
	
	}
    }
    catch (e)
    {
	FireOTR.debug (e);
    }
    
    
};
FireOTR.isFragmented = function (msg_text)
{

    return  (msg_text.search (/\?OTR\,\d/) != -1);
    
}
FireOTR.processingReceivingMessage = function (name, protocol, from, msg_text)
{
    
    try{
	/* fb seams doesn't deliver messages in the right order.  We have to
	   assure fragmented message to arrive in right order
	   otherwise libotr doesn't work */
	from = from.toString();
	if (!FireOTR.isFragmented (msg_text))
	    {
		FireOTR._processingReceivingMessage (name, protocol, from, msg_text);
		return;
	    }
	var context= FireOTR.libotr.otrl_context_find (FireOTR.user_state, 
						       from.toString(),
						       name,
						       protocol,
						       0,
						       null,
						       null,
						       null);
	var k = context.contents.fragment_k;
	var n = context.contents.fragment_n;
	var msgK = msg_text.match (/^\?OTR,(\d*),/)[1];

	if (msgK == k+1)
	{
	    FireOTR._processingReceivingMessage (name, protocol, from, msg_text);
	    if (!FireOTR.context[name+protocol+from])
		return;

	    hole = false;
	    for (i = msgK+1;i < n+1 && !hole;i++)
	    {
		if (FireOTR.context[name+protocol+from].messages[i])
		{
		    FireOTR._processingReceivingMessage (name, protocol, from, FireOTR.context[name.procol+from].messages[i]);
		    delete FireOTR.context[name+protocol+from].messages[i];
		}
		else
		    hole = true;
	    }

	}
	else if (msgK > k+1)
	{
	    if (!FireOTR.context[name+protocol+from])
	    {
		FireOTR.context[name+protocol+from] = {};
		FireOTR.context[name+protocol+from].messages = new Array (); 
	    }
	    
	    FireOTR.context[name+protocol+from].messages[msgK] = msg_text;
	}
    }
    catch (e)
    {
	FireOTR.debug (e);
    }
};

FireOTR._processingReceivingMessage = function (name, protocol, from, msg_text)
{

    try{
	FireOTR.debug ('processing recv message: '+msg_text);
	var newMessage = new ctypes.char.ptr (null);
	var ignore_message = FireOTR.libotr.otrl_message_receiving (FireOTR.user_state,
								   FireOTR.uiOps.address (),
								   null,
								   name,
								   protocol,
								   from,
								   msg_text,
								   newMessage.address(),
								   null,
								   null,
								   null);


	if (!ignore_message)
	{
	    if (FireOTR.handlers[protocol])
	    {
		h = FireOTR.handlers[protocol];
		h.displayMsg (from,  newMessage.isNull() ? msg_text : newMessage.readString ());
	    }
	    FireOTR.libotr.otrl_message_free (newMessage);	   
	}
	return ignore_message;
    }
    catch (e)
    {
	FireOTR.debug (e);
    }
    
};
/* CALLBACKS */

/*STUB*/
FireOTR.policy_cb = function (opdata, context)
{

    var ret = new ctypes.unsigned_int (59);
    return ret; //OPPORTUNISTIC;
}

FireOTR.create_privkey_cb = function (opdata, accountName, protocol)
{
    try{
	alert ("We're going to create a private key. We need some time  and some entropy to do that. You can generate entropy moving you mouse or hitting your keybord after press the Ok button. A message will notify you when the key is created");
	var workerFactory = Components.classes["@mozilla.org/threads/workerfactory;1"]
	                              .createInstance(Components.interfaces.nsIWorkerFactory);
	 
	var worker = workerFactory.newChromeWorker("chrome://fireotr/content/create_privkey.js");
//	var worker = new ChromeWorker("chrome://fireotr/content/create_privkey.js");
	var privkey_file = FileUtils.getFile("ProfD", ["otr.private_key"]);
	var filename = privkey_file.path;
	
	var msg = {};

	msg.accountName = accountName.readString ();
	msg.protocol = protocol.readString ();
	msg.filename = filename;
	//msg.otrl_privkey_generate = FireOTR.libotr.otrl_privkey_generate;
	msg.user_state = FireOTR.user_state.toString();

	worker.onmessage = function (message)
	{
	    FireOTR.debug (message.data);
	    if (message.data == 0)
		alert ('Private key created');
	}
	/* i don't know if it works without real locks objects */

	worker.postMessage (msg);


    }
    catch(e)
    {
	alert (e);
    }
}


//STUB
FireOTR.is_logged_in_cb = function (opdata, accountName, protocol, recipient)
{
    return 1;
}

FireOTR.inject_message_cb = function (opdata, accountname, protocol, recipient, message)
{
    try{
	if (FireOTR.handlers[protocol.readString()])
	{
	    h = FireOTR.handlers[protocol.readString()];
	    h.enqueueMessage (recipient.readString(), message.readString());	
	}
    }
    catch(e)
    {
	FireOTR.debug (e);
    }
    
}

//STUB
FireOTR.notify_cb = function (opdata, level,	accountname, protocol,
				username, title, primary, secondary)
{
    alert ('notify cb');
    alert (title.readString ());
    alert (primary.readString ());
}


//STUB
FireOTR.display_otr_message_cb = function (opdata, accountname, protocol, username, msg)
{
    FireOTR.debug ('display cb');
    alert (msg.readString ());
    return 0;
}
//STUB
FireOTR.protocol_name_cb = function (opdata, protocol)
{
    alert ('protocol name cb')
    return 'facebook';

}
FireOTR.protocol_name_free_cb = function (opdata, protocol)
{
    FireOTR.debug ('protocol name free');
}

FireOTR.new_fingerprint_cb = function (opdata, userstate, accountname, protocol, username, fingerprint)
{
    alert ('new fingerprint');
    try{
	var prompts = Cc["@mozilla.org/embedcomp/prompt-service;1"].
	    getService(Ci.nsIPromptService);

	var jsUserName = username.readString ();
	var h = FireOTR.handlers[protocol.readString ()];
	var name = (h && h.names[jsUserName ]) ? h.names[jsUserName] : jsUserName;
	
	var hfp = new ctypes.ArrayType(ctypes.char, 45)();

	FireOTR.libotr.otrl_privkey_hash_to_human (hfp, fingerprint);

	if (prompts.confirm (window, "Fingerprint verification", "Do you want to trust fingerpint "+hfp.readString()+" for user "+name+"?"))
	{
	    var context = FireOTR.libotr.otrl_context_find (userstate, username, accountname, protocol, 0, null, null, null);
	    if (!context.isNull())
		var fp = FireOTR.libotr.otrl_context_find_fingerprint (context, fingerprint, 0, null);
	    if (!fp.isNull())
		FireOTR.libotr.otrl_context_set_trust (fp, "verified");
	}
    }
    catch (e)
    {
	FireOTR.debug (e);
    }
}

FireOTR.write_fingerprint_cb = function (opdata)
{
    try {
	alert ('WRITING A NEW FINGERPRINT');
	var fingerprints_file = FileUtils.getFile("ProfD", ["otr.fingerprints"]);
	FireOTR.libotr.otrl_privkey_write_fingerprints (FireOTR.user_state, fingerprints_file.path);
    }
    catch (e)
    {
	FireOTR.debug ('some error occured writing fingerprints to disk '+e);
    }
}

FireOTR.update_context_list_cb = function (opdata)
{
    
}

FireOTR.gone_secure_cb = function (opdata, context)
{
    FireOTR.debug ('gone secure');
    var protocol = context.contents.protocol.readString ();
    var h = FireOTR.handlers[protocol];
    if (!h)
    {
	FireOTR.debug ("Not handler for protocol "+protocol);
	return;
    }
    FireOTR.debug (h);
    FireOTR.debug (h.gone_secure);
    h.gone_secure (opdata, context);
}
FireOTR.gone_insecure_cb = function (opdata, context)
{
    alert ('gone no secure');
    var protocol = context.contents.protocol.readString ();

    var h = FireOTR.handlers[protocol];
    if (!h)
    {
	FireOTR.debug ("Not handler for protocol "+protocol);
	return;
    }
    h.gone_insecure (opdata, context);
}

FireOTR.still_secure_cb = function (opdata, context, is_reply)
{
    alert ('still secure bo');
}
FireOTR.log_message_cb = function (opdata, message)
{
    FireOTR.debug ('log message cb '+message.readString());
}

FireOTR.max_message_size_cb = function (opdata, context)
{
    return 500;
}


FireOTR.init = function()
{
    
    try{

	/* Every handler will do FireOTR.handlers.push */
	
	window.addEventListener ("load", FireOTR.onChromeLoad, false);

	FireOTR.libotr = new otr_wrapper ();

	FireOTR.libotr.init (3, 2, 0);
	/* we will use only one user_state */
	FireOTR.user_state = FireOTR.libotr.otrl_userstate_create ();

	Components.utils.import("resource://gre/modules/FileUtils.jsm");

	var privkey_file = FileUtils.getFile("ProfD", ["otr.private_key"]);
	var fingerprints_file = FileUtils.getFile("ProfD", ["otr.fingerprints"]);
	
	if (!privkey_file.exists ())
	    privkey_file.create (0, 0600);
	if (!fingerprints_file.exists ())
	    fingerprints_file.create (0, 0600);
 

	var err = FireOTR.libotr.otrl_privkey_read (FireOTR.user_state, privkey_file.path);
	if (err != 0)
	    alert ('some error reading privkey' + err);
	
	err = FireOTR.libotr.otrl_privkey_read_fingerprints (FireOTR.user_state, fingerprints_file.path, null, null);

	if (err != 0)
	    alert ('some error reading fingerprints' + err);


	policy_cb_ptr = FireOTR.libotr.policy_cb_t (FireOTR.policy_cb);
	create_privkey_cb_ptr = FireOTR.libotr.create_privkey_cb_t (FireOTR.create_privkey_cb);
	is_logged_in_cb_ptr = FireOTR.libotr.is_logged_in_cb_t (FireOTR.is_logged_in_cb);
	inject_message_cb_ptr = FireOTR.libotr.inject_message_cb_t (FireOTR.inject_message_cb);
	notify_cb_ptr = FireOTR.libotr.notify_cb_t (FireOTR.notify_cb);
	display_otr_message_cb_ptr = FireOTR.libotr.display_otr_message_cb_t (FireOTR.display_otr_message_cb);
	update_context_list_cb_ptr = FireOTR.libotr.update_context_list_cb_t (FireOTR.update_context_list_cb);
	protocol_name_cb_ptr = FireOTR.libotr.protocol_name_cb_t (FireOTR.protocol_name_cb);
	protocol_name_free_cb_ptr = FireOTR.libotr.protocol_name_free_cb_t (FireOTR.protocol_name_free_cb);
	new_fingerprint_cb_ptr = FireOTR.libotr.new_fingerprint_cb_t (FireOTR.new_fingerprint_cb);
	write_fingerprint_cb_ptr = FireOTR.libotr.write_fingerprint_cb_t (FireOTR.write_fingerprint_cb);
	gone_secure_cb_ptr = FireOTR.libotr.gone_secure_cb_t (FireOTR.gone_secure_cb);
	gone_insecure_cb_ptr = FireOTR.libotr.gone_insecure_cb_t (FireOTR.gone_insecure_cb);
	still_secure_cb_ptr = FireOTR.libotr.still_secure_cb_t (FireOTR.still_secure_cb);
	log_message_cb_ptr = FireOTR.libotr.log_message_cb_t (FireOTR.log_message_cb);
	max_message_size_cb_ptr = FireOTR.libotr.max_message_size_cb_t (FireOTR.max_message_size_cb);
	

	FireOTR.uiOps = FireOTR.libotr.OtrlMessageAppOps_t ();
	FireOTR.uiOps.policy = policy_cb_ptr;
	FireOTR.uiOps.create_privkey = create_privkey_cb_ptr;
	FireOTR.uiOps.is_logged_in = is_logged_in_cb_ptr;
	FireOTR.uiOps.inject_message = inject_message_cb_ptr;
	FireOTR.uiOps.notify = notify_cb_ptr;
	FireOTR.uiOps.display_otr_message = display_otr_message_cb_ptr;
	FireOTR.uiOps.update_context_list = update_context_list_cb_ptr;
	FireOTR.uiOps.protocol_name = protocol_name_cb_ptr;
	FireOTR.uiOps.protocol_name_free = protocol_name_free_cb_ptr;
	FireOTR.uiOps.new_fingerprint = new_fingerprint_cb_ptr;
	FireOTR.uiOps.write_fingerprint = write_fingerprint_cb_ptr;
	FireOTR.uiOps.gone_secure = gone_secure_cb_ptr;
	FireOTR.uiOps.gone_insecure = gone_insecure_cb_ptr;
	FireOTR.uiOps.still_secure = still_secure_cb_ptr;
	FireOTR.uiOps.log_message = log_message_cb_ptr;
	FireOTR.uiOps.max_message_size = max_message_size_cb_ptr;


    }
    catch (e)
    {

	alert ('Error initializing libotr and/or reading profiles files');
	alert (e);
    }

};

FireOTR.onChromeLoad = function (event)
{
    try
    {
	document.getElementById("appcontent").addEventListener("DOMContentLoaded", FireOTR.onDOMContentLoad, true);
    }
    catch(e)
    {
	alert (e);
    }
};

FireOTR.onDOMContentLoad = function (event)
{

    var doc = event.originalTarget;

    for each (var h in FireOTR.handlers)
	if (doc.location.hostname.match (h.url_matching))
	    h.onPageLoad (event);
};




try 
{
    FireOTR.init ();

}
catch (e)
{
    alert (e);
}
// try 
//     {
// 	FireOTR.libotr = new otr_wrapper ();
// 	FireOTR.libotr.init (3, 2, 0); //we have to read version somewhere?
// 	FireOTR.user_state = FireOTR.libotr.otrl_userstate_create ();

// 	Components.utils.import("resource://gre/modules/FileUtils.jsm");
// 	// get the "data.txt" file in the profile directory
// 	var privkey_file = FileUtils.getFile("ProfD", ["otr.private_key"]);
// 	var fingerprints_file = FileUtils.getFile("ProfD", ["otr.fingerprints"]);

// 	var err = FireOTR.libotr.otrl_privkey_read (FireOTR.user_state, privkey_file.path);
// 	if (err != 0)
// 	    alert ('some error reading privkey' + err);

// 	err = FireOTR.libotr.otrl_privkey_read_fingerprints (FireOTR.user_state, fingerprints_file.path, null, null);
// 	if (err != 0)
// 	    alert ('some error reading fingerprints' + err);
        
//     }
// catch (e){
//     alert (e);
// }





