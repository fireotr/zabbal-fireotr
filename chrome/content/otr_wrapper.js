if (!ff_otr)
    var ff_otr = {};

function otr_wrapper ()
{
    try
    {
	Components.utils.import("resource://gre/modules/ctypes.jsm")
	Components.utils.import("resource://gre/modules/Services.jsm");

	var appInfoServ = Services.appinfo;
	var OS = appInfoServ.OS;
	var libName = "";

	if (OS == 'Linux')
	{
	    libName = "libotr.so";
	    var abi = ctypes.default_abi;
	}
	else if (OS == 'Windows')
	{
	    libName = "libotr.dll";
	    var abi = ctypes.stdcall_abi;
	}


	var libotr = ctypes.open (libName);
	var s_OtrlUserState = new ctypes.StructType ("s_OtrlUserState");

	this.time_t = ctypes.long; //mm.../usr/include/bits/type.h and /usr/include/bit/typesize.h

	this.OtrlUserState_t = ctypes.PointerType (s_OtrlUserState);
	this.ConnContext_t = ctypes.StructType ("ConnContext");
	this.s_fingerprint_t = ctypes.StructType ("s_fingerprint");

	/*enum*/
	this.OtrlMessageState_t = ctypes.int; 
	this.OtrlAuthState_t = ctypes.int; 
	this.OtrlSessionIdHalf = ctypes.int;
	this.NextExpectedSMP = ctypes.int;
	this.OtrlSMProgState = ctypes.int;
	this.OtrlNotifyLevel_t = ctypes.int;
	this.OtrlFragmentPolicy_t = ctypes.int;


	this.OTRL_FRAGMENT_SEND_ALL = 0;
	this.OTRL_FRAGMENT_SEND_ALL_BUT_FIRST = 1;
	this.OTRL_FRAGMENT_SEND_ALL_BUT_LAST = 2;

	this.OtrlTLV = ctypes.StructType ("s_OtrlTLV"); //opaque
	this.gcry_mpi = ctypes.StructType ("gcry_mpi");//gcrypt.h L344
	this.gcry_mpi_t = this.gcry_mpi.ptr;
	this.gcry_cipher_handle = ctypes.StructType ("gcry_cipher_handle");//gcrypt L801
	this.gcry_cipher_hd_t = this.gcry_cipher_handle.ptr;
	this.gcry_md_context = ctypes.StructType ("gcry_md_context");//gcrypt L1094 forward declaration
	this.gcry_md_hd_t = ctypes.StructType ("gcry_md_handle", //gcrypt L1101
					       [{"ctx":this.gcry_md_context.ptr},
						{"bufpos":ctypes.int},
						{"bufsize":ctypes.int},
						{"buf":ctypes.unsigned_char.array(1)}]).ptr;
						
	this.DH_keypair_t = ctypes.StructType ("DH_keypair",
					       [{"groupid":ctypes.unsigned_int},
						{"priv":this.gcry_mpi_t},
						{"pub":this.gcry_mpi_t}]);

	this.OtrlAuthInfo_t = ctypes.StructType ("OtrlAuthInfo",
						 [{"authstate":this.OtrlAuthState_t},
						  {"our_dh":this.DH_keypair_t},
						  {"our_keyid":ctypes.unsigned_int},
						  {"encgx":ctypes.unsigned_char.ptr},
						  {"encgx_len":ctypes.size_t},
						  {"r":ctypes.unsigned_char.array(16)},
						  {"hashgx":ctypes.unsigned_char.array(32)},
						  {"their_pub":this.gcry_mpi_t},
						  {"their_keyid":ctypes.unsigned_int},
						  {"enc_c":this.gcry_cipher_hd_t},
						  {"enc_cp":this.gcry_cipher_hd_t},
						  {"mac_m1":this.gcry_md_hd_t},
						  {"mac_m1p":this.gcry_md_hd_t},
						  {"mac_m2":this.gcry_md_hd_t},
						  {"mac_m2p":this.gcry_md_hd_t},
						  {"their_fingerprint":ctypes.unsigned_char.array(20)},
						  {"initiated":ctypes.int},
						  {"protcol_version":ctypes.unsigned_int},
						  {"secure_session_id":ctypes.unsigned_char.array(20)},
						  {"secure_session_id_len":ctypes.size_t},
						  {"session_id_half": this.OtrlSessionIdHalf},
						  {"lastauthmsg": ctypes.char.ptr}]);


	this.s_fingerprint_t.define ([{"next":this.s_fingerprint_t.ptr},
				      {"tous":this.s_fingerprint_t.ptr.ptr},
				      {"fingerprint":ctypes.char.ptr},
				      {"context":this.ConnContext_t.ptr},
				      {"trust": ctypes.char.ptr}]);


	this.Fingerprint_t = this.s_fingerprint_t;

	this.DH_sesskeys_t = ctypes.StructType ("DH_sesskeys",
					       [{"sendctr": ctypes.unsigned_char.array (16)},
						{"rcvctr": ctypes.unsigned_char.array (16)},
	 				       	{"sendenc": this.gcry_cipher_hd_t},
	 				       	{"rcvenc": this.gcry_cipher_hd_t},
	 				       	{"sendmac": this.gcry_md_hd_t},
	 				       	{"sendmackey": ctypes.unsigned_char.array (20)},
	 				       	{"sendmacused": ctypes.int},
	 				       	{"rcvmac": this.gcry_md_hd_t},
					       	{"rcvmackey": ctypes.unsigned_char.array (20)},
					       	{"rcvmacused": ctypes.int}]);


	this.OtrlPolicy_t = ctypes.unsigned_int;

	this.OtrlSMState = ctypes.StructType ("OtrlSMState",
					      [{"secret": this.gcry_mpi_t}, 
					       {"x2": this.gcry_mpi_t}, 
					       {"x3": this.gcry_mpi_t}, 
					       {"g1": this.gcry_mpi_t}, 
					       {"g2": this.gcry_mpi_t}, 
					       {"g3": this.gcry_mpi_t}, 
					       {"g3o": this.gcry_mpi_t}, 
					       {"p": this.gcry_mpi_t}, 
					       {"q": this.gcry_mpi_t}, 
					       {"pab": this.gcry_mpi_t}, 
					       {"qab": this.gcry_mpi_t}, 
					       {"nextExpected": this.NextExpectedSMP},
					       {"received_question": ctypes.int},
					       {"sm_prog_state": this.OtrlSMProgState}]);

	this.app_data_free_t = ctypes.FunctionType (abi,
						    ctypes.void_t,
						    [ctypes.void_t.ptr]);

	 this.ConnContext_t.define ([{"next": this.ConnContext_t.ptr},
				     {"tous": this.ConnContext_t.ptr.ptr},
				     {"username": ctypes.char.ptr},
				     {"accountname": ctypes.char.ptr},
				     {"protocol": ctypes.char.ptr},
				     {"fragment": ctypes.char},
				     {"fragment_len": ctypes.size_t},
				     {"fragment_n": ctypes.unsigned_short},
				     {"fragment_k": ctypes.unsigned_short},
				     {"msgstate": this.OtrlMessageState_t},
				     {"auth": this.OtrlAuthInfo_t},
				     {"fingerprint_root": this.Fingerprint_t},
				     {"active_fingerprint": this.Fingerprint_t.ptr},
				     {"their_keyid": ctypes.unsigned_int},
				     {"their_y": this.gcry_mpi_t},
				     {"their_old_y": this.gcry_mpi_t},
				     {"our_keyid": ctypes.unsigned_int},
				     {"our_dh_key": this.DH_keypair_t},
				     {"our_old_dh_key": this.DH_keypair_t},
				     {"sesskeys": this.DH_sesskeys_t.array(2).array(2)},
				     {"sessionid": ctypes.unsigned_char.array (20)},
				     {"sessionid_len": ctypes.size_t},
				     {"sessionid_half": this.OtrlSessionIdHalf},
				     {"protocol_version": ctypes.unsigned_int},
				     {"preshared_secret": ctypes.unsigned_char.ptr},
				     {"preshared_secret_len": ctypes.size_t},
				     {"numsavedkeys": ctypes.unsigned_int},
				     {"saved_mac_keys": ctypes.unsigned_char.ptr},
				     {"generation": ctypes.unsigned_int},
				     {"lastsent": this.time_t},
				     {"lastmessage": ctypes.char.ptr},
				     {"may_retransmit": ctypes.int},
				     {"otr_offer": ctypes.int},
				     {"app_data": ctypes.void_t.ptr},
				     {"app_data_free": this.app_data_free_t.ptr},
	 			     {"smstate": this.OtrlSMState.ptr}]);

	this.init = libotr.declare ("otrl_init", 
				    abi, 
				    ctypes.void_t,
				    ctypes.uint32_t,
				    ctypes.uint32_t,
				    ctypes.uint32_t);

	this.otrl_userstate_create = libotr.declare ("otrl_userstate_create",
						     abi,
						     this.OtrlUserState_t);

	this.add_appdata_t = ctypes.FunctionType (abi,
						    ctypes.void_t,
						  [ctypes.void_t.ptr,
						  this.ConnContext_t.ptr]);


	this.otrl_privkey_read = libotr.declare ("otrl_privkey_read",
						 abi,
						 ctypes.unsigned_int,
						 this.OtrlUserState_t,
						 ctypes.char.ptr);

	this.otrl_privkey_read_fingerprints = libotr.declare ("otrl_privkey_read_fingerprints",
							      abi,
							      ctypes.unsigned_int,
							      this.OtrlUserState_t,
							      ctypes.char.ptr,
							      ctypes.void_t.ptr,
							      ctypes.void_t.ptr);

	this.otrl_privkey_generate = libotr.declare ("otrl_privkey_generate",
						     abi,
						     ctypes.unsigned_int,
						     this.OtrlUserState_t,
						     ctypes.char.ptr,
						     ctypes.char.ptr,
						     ctypes.char.ptr);
	
	//callback types
	this.policy_cb_t = ctypes.FunctionType (abi,
						this.OtrlPolicy_t,
						[ctypes.void_t.ptr,
						this.ConnContext_t.ptr]).ptr;

	this.create_privkey_cb_t = ctypes.FunctionType (abi,
							ctypes.void_t,
							[ctypes.void_t.ptr,
							ctypes.char.ptr,
							ctypes.char.ptr]).ptr;

	this.is_logged_in_cb_t = ctypes.FunctionType (abi,
						      ctypes.int,
						      [ctypes.void_t.ptr,
						      ctypes.char.ptr,
						      ctypes.char.ptr,
						      ctypes.char.ptr]).ptr;

	this.inject_message_cb_t = ctypes.FunctionType (abi,
							ctypes.void_t,
							[ctypes.void_t.ptr,
							ctypes.char.ptr,
							ctypes.char.ptr,
							ctypes.char.ptr,
							ctypes.char.ptr]).ptr;

	this.notify_cb_t = ctypes.FunctionType (abi,
						ctypes.void_t,
						[ctypes.void_t.ptr,
						this.OtrlNotifyLevel_t,
						ctypes.char.ptr,
						ctypes.char.ptr,
						ctypes.char.ptr,
						ctypes.char.ptr,
						ctypes.char.ptr,
						ctypes.char.ptr]).ptr;

	this.display_otr_message_cb_t = ctypes.FunctionType (abi,
							     ctypes.int,
							     [ctypes.void_t.ptr,
							     ctypes.char.ptr,
							     ctypes.char.ptr,
							     ctypes.char.ptr,
							     ctypes.char.ptr]).ptr;

	this.update_context_list_cb_t = ctypes.FunctionType (abi,
							     ctypes.void_t,
							     [ctypes.void_t.ptr]).ptr;
	this.protocol_name_cb_t = ctypes.FunctionType (abi,
						      ctypes.char.ptr,
						      [ctypes.void_t.ptr,
						       ctypes.char.ptr]).ptr;
	
	this.protocol_name_free_cb_t = ctypes.FunctionType (abi,
							    ctypes.void_t,
							    [ctypes.void_t.ptr,
							     ctypes.char.ptr]).ptr;
	
	this.new_fingerprint_cb_t = ctypes.FunctionType (abi,
						    ctypes.void_t,
						    [ctypes.void_t.ptr,
						     this.OtrlUserState_t,
						     ctypes.char.ptr,
						     ctypes.char.ptr,
						     ctypes.char.ptr,
						     ctypes.ArrayType (ctypes.unsigned_char, 20)]).ptr;

	this.write_fingerprint_cb_t = ctypes.FunctionType (abi,
							   ctypes.void_t,
							   [ctypes.void_t.ptr]).ptr;

	this.gone_secure_cb_t = ctypes.FunctionType (abi, 
						     ctypes.void_t,
						     [ctypes.void_t.ptr,
						      this.ConnContext_t.ptr]).ptr;

	this.gone_insecure_cb_t = ctypes.FunctionType (abi, 
						     ctypes.void_t,
						     [ctypes.void_t.ptr,
						      this.ConnContext_t.ptr]).ptr;
	

	this.still_secure_cb_t = ctypes.FunctionType (abi,
						      ctypes.void_t,
						      [ctypes.void_t.ptr,
						       this.ConnContext_t.ptr,
						       ctypes.int]).ptr;

	this.log_message_cb_t = ctypes.FunctionType (abi,
						     ctypes.void_t,
						     [ctypes.void_t.ptr,
						      ctypes.char.ptr]).ptr;

	this.max_message_size_cb_t = ctypes.FunctionType (abi,
							  ctypes.int,
							  [ctypes.void_t.ptr,
							   this.ConnContext_t.ptr]).ptr;

	this.account_name_cb_t = ctypes.FunctionType (abi,
						      ctypes.char.ptr,
						      [ctypes.void_t.ptr,
						       ctypes.char.ptr]).ptr;

	this.account_name_free_cb_t = ctypes.FunctionType (abi,
							   ctypes.void_t,
							   [ctypes.void_t.ptr,
							    ctypes.char.ptr]).ptr;
							   

						  
	this.OtrlMessageAppOps_t = ctypes.StructType ("s_OtrlMessageAppOps",
						      [{policy : this.policy_cb_t},
						       {create_privkey: this.create_privkey_cb_t},
						       {is_logged_in:this.is_logged_in_cb_t},
						       {inject_message:this.inject_message_cb_t},
						       {notify:this.notify_cb_t},
						       {display_otr_message:this.display_otr_message_cb_t},
						       {update_context_list:this.update_context_list_cb_t},
						       {protocol_name:this.protocol_name_cb_t},
						       {protocol_name_free:this.protocol_name_free_cb_t},
						       {new_fingerprint:this.new_fingerprint_cb_t},
						       {write_fingerprint:this.write_fingerprint_cb_t},
						       {gone_secure: this.gone_secure_cb_t},
						       {gone_insecure: this.gone_insecure_cb_t},
						       {still_secure:this.still_secure_cb_t},
						       {log_message:this.log_message_cb_t},
						       {max_message_size:this.max_message_size_cb_t},
						       {account_name:this.account_name_cb_t},
						       {account_name_free:this.account_name_free_cb_t}]);

	this.otrl_message_sending = libotr.declare ("otrl_message_sending",
						    abi,
						    ctypes.unsigned_int,
						    this.OtrlUserState_t,
						    this.OtrlMessageAppOps_t.ptr,
						    ctypes.void_t.ptr,
						    ctypes.char.ptr,
						    ctypes.char.ptr,
						    ctypes.char.ptr,
						    ctypes.char.ptr,
						    this.OtrlTLV.ptr,
						    ctypes.char.ptr.ptr,
						    this.add_appdata_t.ptr,
						    ctypes.void_t.ptr);

	this.otrl_message_fragment_and_send = libotr.declare ("otrl_message_fragment_and_send",
					     abi,
					     ctypes.unsigned_int,
							      this.OtrlMessageAppOps_t.ptr,
					     ctypes.void_t.ptr,
					     this.ConnContext_t.ptr,
					     ctypes.char.ptr,
					     this.OtrlFragmentPolicy_t,
					     ctypes.char.ptr.ptr);

	this.otrl_context_find = libotr.declare ("otrl_context_find",
				abi,
				this.ConnContext_t.ptr,
				this.OtrlUserState_t,
				ctypes.char.ptr,
				ctypes.char.ptr,
				ctypes.char.ptr,
				ctypes.int,
				ctypes.int.ptr,
				this.add_appdata_t.ptr,
				ctypes.void_t.ptr);

	this.otrl_message_receiving = libotr.declare ("otrl_message_receiving",
						      abi,
						      ctypes.int,
						      this.OtrlUserState_t,
						      this.OtrlMessageAppOps_t.ptr,
						      ctypes.void_t.ptr,
						      ctypes.char.ptr,
						      ctypes.char.ptr,
						      ctypes.char.ptr,
						      ctypes.char.ptr,
						      ctypes.char.ptr.ptr,
						      this.OtrlTLV.ptr.ptr,
						      this.add_appdata_t.ptr, //portare add_app_data_t del tipo ptr TODO
						      ctypes.void_t.ptr);
	this.otrl_message_free = libotr.declare ("otrl_message_free",
						  abi,
						  ctypes.void_t,
						  ctypes.char.ptr);

	this.otrl_proto_default_query_msg = libotr.declare ("otrl_proto_default_query_msg",
					   abi,
					   ctypes.char.ptr,
					   ctypes.char.ptr,
					   this.OtrlPolicy_t);

	this.otrl_context_find = libotr.declare ("otrl_context_find",
						 abi,
						 this.ConnContext_t.ptr,
						 this.OtrlUserState_t,
						 ctypes.char.ptr,
						 ctypes.char.ptr,
						 ctypes.char.ptr,
						 ctypes.int,
						 ctypes.int.ptr,
					 	 this.add_appdata_t.ptr, //portare add_app_data_t del tipo ptr TODO
						 ctypes.void_t.ptr);

	this.otrl_privkey_write_fingerprints  = libotr.declare ("otrl_privkey_write_fingerprints",
								abi,
								ctypes.unsigned_int,
								this.OtrlUserState_t,
								ctypes.char.ptr);

	this.otrl_context_find_fingerprint = libotr.declare ("otrl_context_find_fingerprint",
							     abi,
							     this.Fingerprint_t.ptr,
							     this.ConnContext_t.ptr,
							     ctypes.ArrayType (ctypes.unsigned_char, 20),
							     ctypes.int,
							     ctypes.int.ptr);

	this.otrl_context_set_trust = libotr.declare ("otrl_context_set_trust",
						      abi,
						      ctypes.void_t,
						      this.Fingerprint_t.ptr,
						      ctypes.char.ptr);

	this.otrl_privkey_hash_to_human = libotr.declare ("otrl_privkey_hash_to_human",
							  abi,
							  ctypes.void_t,
							  ctypes.ArrayType(ctypes.char, 45),
							  ctypes.ArrayType(ctypes.unsigned_char, 20));

	this.otrl_privkey_fingerprint = libotr.declare ("otrl_privkey_fingerprint",
							abi,
							ctypes.char.ptr,
							this.OtrlUserState_t.ptr,
							ctypes.char.array(45),
							ctypes.char.ptr,
							ctypes.char.ptr);
							
	this.otrl_message_disconnect  = libotr.declare ("otrl_message_disconnect",
							abi,
							ctypes.void_t,
							this.OtrlUserState_t,
  							this.OtrlMessageAppOps_t.ptr,
							ctypes.void_t.ptr,
							ctypes.char.ptr,
							ctypes.char.ptr,
							ctypes.char.ptr);
				      
	this.otrl_context_force_plaintext = libotr.declare ("otrl_context_force_plaintext",
							    abi,
							    ctypes.void_t,
							    this.ConnContext_t.ptr);

				    
	

    }
    catch (e)
    {
	alert ("Some error importing libotr symbols: " + e + " at line:" + e.lineNumber);
    }
}


