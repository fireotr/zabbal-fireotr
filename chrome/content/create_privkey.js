onmessage = function (message)
{
    var accountName = message.data.accountName;
    var protocol = message.data.protocol;
    var filename = message.data.filename;
    var us = message.data.user_state;

    var libName = "libotr.so";
    var abi = ctypes.default_abi;
    
    var libotr = ctypes.open (libName);
    var s_OtrlUserState = new ctypes.StructType ("s_OtrlUserState");
    var OtrlUserState_t = ctypes.PointerType (s_OtrlUserState);

//	Components.utils.import("resource://gre/modules/FileUtils.jsm"); //we already done???

    var otrl_privkey_generate = libotr.declare ("otrl_privkey_generate",
						abi,
						ctypes.unsigned_int,
						OtrlUserState_t,
						ctypes.char.ptr,
						ctypes.char.ptr,
						ctypes.char.ptr);

    
    /* We have to detect arch here and cast from UInt32 in case of 32 bit arch */
    var arch32 = us.search (/Uint32/);


    if (arch32 != -1)
	var userState = new OtrlUserState_t (ctypes.UInt32(parseInt(us.match (/0x[\dabcdef]*/))));
    else
	var userState = new OtrlUserState_t (ctypes.UInt64(parseInt(us.match (/0x[\dabcdef]*/))));
    var err =  otrl_privkey_generate (userState, filename, accountName, protocol);
    postMessage (err);

}



